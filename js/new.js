const root = document.querySelector("#root");
const ul = document.createElement("ul");

root.append(ul);

const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

let listBooks = books.filter((e) => {
  try{
    if(!e.author) {
      throw new Error(`${e}: Не вказаний автор`);
    }
    if(!e.name) {
      throw new Error(`${e}: Не вказана назва книги`);
    }
    if(!e.price) {
      throw new Error(`В елементі ${e} не вказана ціна`);
    }

    return e;
  } catch(err) {
    console.warn(`Error: `, err.message);

    return false
  }
})

listBooks.forEach((el) => {
  const li = document.createElement("li")
  li.textContent = `Автор: ${el.author}, Назва книги: ${el.name}, Вартість: ${el.price} грн`;
  ul.append(li);
})